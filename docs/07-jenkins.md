# Jenkins Workshop

## Prepare your environment in Jenkins

* Go to <https://jenkins.demo.opsta.co.th> and login with supplied credential
* Click on folder DevOps for Developer #[X]
* Click on `New Item` on the left side
* Enter an item name `training[X]` and choose `Folder` type
* Just leave all the settings default and click on `training[X]` at breadcrumb
* Make sure you are in `training[X]` folder and click on `New Item` on the left side
* Enter an item name `ratings` and choose `Folder` type
* Just leave all the settings default and click on `ratings` at breadcrumb

## Prepare Jenkins Credentials

### Prepare Deploy Keys

* On Cloud Shell put below commands

```bash
mkdir -p ~/infra/deploy-keys/
ssh-keygen -f ~/infra/deploy-keys/id_rsa -N ""
cat ~/infra/deploy-keys/id_rsa.pub
# Copy Public Key
```

### Setup Deploy Keys on GitLab

* Go to <https://git.demo.opsta.co.th/dfd-[Y]/training[X]/ratings/-/settings/repository> and click `Expand` on `Deploy Keys`
* Input deploy key information
  * Title: `training[X]-bookinfo@jenkins-opsta-demo`
  * Key: Put your copied public key
  * Write access allowed: checked

### Create Jenkins Credentials

* Make sure you are in your `training[X]` folder and click on `Credentials` > `Folder` respectively on the left side
* Click on `Global credentials (unrestricted)`
* Click on `Add Credentials` on the left side
* On Cloud Shell copy your private key with command `cat ~/infra/deploy-keys/id_rsa`
* Create Bookinfo Deploy Key Credential
  * Kind: `SSH Username with private key`
  * ID: `training[X]-bookinfo-git-deploy-key`
  * Description: `training[X]-bookinfo-git-deploy-key`
  * Username: `git`
  * Private Key: `Enter directly` and add your copied private key
* Create Bookinfo Nexus Docker Private Registry Credential
  * Kind: `Username with password`
  * Username: `bookinfo`
  * Password: [your-password]
  * ID: `bookinfo-registry`
  * Description: `bookinfo-registry`

## Create first Docker Build and push job

* Click on `New Item` on the left side and make sure you are in your `ratings` folder
* Enter an item name `deploy-dev` and choose `Pipeline` type
* Configure job
  * Advanced Project Options > Advanced...
    * Display Name: `Deploy to Development Environment`
  * Pipeline
    * Definition: `Pipeline script`
    * Script:

```groovy
// Define variables
def scmVars

// Start Pipeline
pipeline {

  // Configure Jenkins Slave
  agent {
    // Use Kubernetes as dynamic Jenkins Slave
    kubernetes {
      // Kubernetes Manifest File to spin up Pod to do build
      yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: docker
    image: docker:19.03.5-dind
    command:
    - dockerd
    - --host=unix:///var/run/docker.sock
    - --host=tcp://0.0.0.0:2375
    - --storage-driver=overlay2
    tty: true
    securityContext:
      privileged: true
"""
    } // End kubernetes
  } // End agent

  // Start Pipeline
  stages {

    // ***** Stage Clone *****
    stage('Clone ratings source code') {
      // Steps to run build
      steps {
        // Run in Jenkins Slave container
        container('jnlp') {
          // Use script to run
          script {
            // Git clone repo and checkout branch as we put in parameter
            scmVars = git branch: 'dev',
                          credentialsId: 'training[X]-bookinfo-git-deploy-key',
                          url: 'git@git.demo.opsta.co.th:dfd-[Y]/training[X]/ratings.git'
          } // End script
        } // End container
      } // End steps
    } // End stage

    // ***** Stage Build *****
    stage('Build ratings Docker Image and push') {
      steps {
        container('docker') {
          script {
            // Do docker login authentication
            docker.withRegistry('https://registry.demo.opsta.co.th', 'registry-bookinfo') {
              // Do docker build and docker push
              docker.build('registry.demo.opsta.co.th/training[X]/bookinfo/ratings:dev').push()
            } // End docker.withRegistry
          } // End script
        } // End container
      } // End steps
    } // End stage

  } // End stages
} // End pipeline
```

* After `Save` then click `Build Now` on the left side

## Add stage Helm deploy

* Add Helm container append to Jenkins Slave

```yaml
  - name: helm
    image: lachlanevenson/k8s-helm:v3.0.2
    command:
    - cat
    tty: true
```

* Add deploy stage after Docker Build stage

```groovy
    // ***** Stage Deploy *****
    stage('Deploy ratings with Helm Chart') {
      steps {
        // Run on Helm container
        container('helm') {
          script {
            // Use kubeconfig from Jenkins Credential
            withKubeConfig([credentialsId: 'gce-k8s-kubeconfig']) {
              // Run Helm upgrade
              sh "helm upgrade -f k8s/helm-values/values-bookinfo-dev-ratings.yaml --wait \
                --namespace training[X]-bookinfo-dev bookinfo-dev-ratings k8s/helm"
            } // End withKubeConfig
          } // End script
        } // End container
      } // End steps
    } // End stage
```

* Click `Build Now`

## Automatically trigger from GitLab

### Configure Build Triggers on Jenkins Job

* Configure Build Triggers
  * Build when a change is pushed to GitLab...: checked
    * Advanced...
      * Allowed branches: `Filter branches by name`
        * Include: `dev`
      * Secret token: `Generate` and copied

### Configure GitLab Webhook

* Go to <https://git.demo.opsta.co.th/dfd-[Y]/training[X]/ratings/-/settings/integrations> or menu `Settings` > `Integrations` in ratings repository
* Setup hook
  * URL: `<https://jenkins.demo.opsta.co.th/project/dfd-[Y]/training[X]/ratings/deploy-dev>`
  * This URL will be triggered by a push to the repository: `dev`
* `Add webhook`

### Try to push from dev branch

* Try to edit `src/ratings.js` line 207 and change healthcheck message to `Ratings is healthy!`
* Commit and push
* You will see in Jenkins that it will build automatically but the code is not change because Helm Chart and Value is the same

### Improve Helm Chart to change every commit

* In Jenkins Pipeline, on `helm upgrade` command put parameter `--set extraEnv.COMMIT_ID=${scmVars.GIT_COMMIT} \`
* In Rating repository, edit `k8s/helm-values/values-bookinfo-dev-ratings.yaml` and add environment variable `COMMIT_ID: CHANGEME`
* Commit and push change

## Exercise: Create Jenkins Job for UAT

* Create `deploy-uat` Jenkins Job
* Configure Pipeline
  * Don't forget to change it to UAT
* Create another hook on GitLab for master branch
* Try to do merge request to test automatically webhook

## Jenkinsfile

* Create `Jenkinsfile` in your rating repository and put below content

```groovy
// Define variables
def scmVars

// Start Pipeline
pipeline {

  // Configure Jenkins Slave
  agent {
    // Use Kubernetes as dynamic Jenkins Slave
    kubernetes {
      // Kubernetes Manifest File to spin up Pod to do build
      yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: docker
    image: docker:19.03.5-dind
    command:
    - dockerd
    - --host=unix:///var/run/docker.sock
    - --host=tcp://0.0.0.0:2375
    - --storage-driver=overlay2
    tty: true
    securityContext:
      privileged: true
  - name: helm
    image: lachlanevenson/k8s-helm:v3.0.2
    command:
    - cat
    tty: true
"""
    } // End kubernetes
  } // End agent

  // Define Environment Variables
  environment {
    ENV_NAME = "${BRANCH_NAME == "master" ? "uat" : "${BRANCH_NAME}"}"
  }

  // Start Pipeline
  stages {

    // ***** Stage Clone *****
    stage('Clone ratings source code') {
      // Steps to run build
      steps {
        // Run in Jenkins Slave container
        container('jnlp') {
          // Use script to run
          script {
            // Git clone repo and checkout branch as we put in parameter
            scmVars = git branch: "${BRANCH_NAME}",
                          credentialsId: 'training[X]-bookinfo-git-deploy-key',
                          url: 'git@git.demo.opsta.co.th:dfd-[Y]/training[X]/ratings.git'
          } // End script
        } // End container
      } // End steps
    } // End stage

    // ***** Stage Build *****
    stage('Build ratings Docker Image and push') {
      steps {
        container('docker') {
          script {
            // Do docker login authentication
            docker.withRegistry('https://registry.demo.opsta.co.th', 'registry-bookinfo') {
              // Do docker build and docker push
              docker.build("registry.demo.opsta.co.th/training[X]/bookinfo/ratings:${ENV_NAME}").push()
            } // End docker.withRegistry
          } // End script
        } // End container
      } // End steps
    } // End stage

    // ***** Stage Deploy *****
    stage('Deploy ratings with Helm Chart') {
      steps {
        // Run on Helm container
        container('helm') {
          script {
            // Use kubeconfig from Jenkins Credential
            withKubeConfig([credentialsId: 'gce-k8s-kubeconfig']) {
              // Run Helm upgrade
              sh "helm upgrade -f k8s/helm-values/values-bookinfo-${ENV_NAME}-ratings.yaml --wait \
                --set extraEnv.COMMIT_ID=${scmVars.GIT_COMMIT} \
                --namespace training[X]-bookinfo-${ENV_NAME} bookinfo-${ENV_NAME}-ratings k8s/helm"
            } // End withKubeConfig
          } // End script
        } // End container
      } // End steps
    } // End stage

  } // End stages
} // End pipeline
```

* Delete all Jenkins Pipeline jobs
* Delete all GitLab webhook
* Create Multibranch Pipeline instead
  * Project Repository: `git@git.demo.opsta.co.th:dfd-[Y]/training[X]/ratings.git`
  * Credentials: `git (training1-bookinfo-git-deploy-key)`
  * Discover branches > Add `Filter by name (with wildcards)`
    * Include: dev master
* Create GitLab webhook
  * URL: `<https://jenkins.demo.opsta.co.th/project/dfd-[Y]/training[X]/ratings/multibranch>`

## Tagging Job

* Click on `New Item` on the left side and make sure you are in your `ratings` folder
* Enter an item name `tagging` and choose `Pipeline` type
* Configure job
  * Advanced Project Options > Advanced...
    * Display Name: `Tagging`
  * Pipeline
    * Definition: `Pipeline script`
    * Script: `Pipeline script from SCM`
      * SCM: `Git`
        * Repository URL: `git@git.demo.opsta.co.th:dfd-[Y]/training[X]/ratings.git`
        * Credentials: `training[X]-bookinfo-git-deploy-key`
      * Script Path: `Jenkinsfile.prd`
* Create new file `Jenkinsfile.tagging`

```groovy
pipeline {
  agent {
    kubernetes {
      yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: docker
    image: docker:19.03.5-dind
    command:
    - dockerd
    - --host=unix:///var/run/docker.sock
    - --host=tcp://0.0.0.0:2375
    - --storage-driver=overlay2
    tty: true
    securityContext:
      privileged: true
  - name: git
    image: alpine/git:1.0.7
    command:
    - cat
    tty: true
"""
    }
  }

  stages {

    // ***** Stage Clone *****
    stage('Clone ratings source code') {
      steps {
        container('jnlp') {
          script {
            git branch: "master",
                credentialsId: 'training[X]-bookinfo-git-deploy-key',
                url: 'git@git.demo.opsta.co.th:dfd-[Y]/training[X]/ratings.git'
          }
        }
      }
    }

    // ***** Stage Docker Tag *****
    stage('Tag Docker Image') {
      steps {
        container('docker') {
          script {
            docker.withRegistry('https://registry.demo.opsta.co.th', 'registry-bookinfo') {
              // Pulling UAT Image
              uatImage = docker.image("registry.demo.opsta.co.th/training[X]/bookinfo/ratings:uat")
              uatImage.pull()
              // Push UAT Image back with build tag
              uatImage.push("build-${BUILD_NUMBER}")
            }
          }
        }
      }
    }

    // ***** Stage Git Tag *****
    stage('Git tag') {
      steps {
        container('git') {
          script {
            // Use deploy key as private key to push code
            sshagent(credentials: ['training[X]-bookinfo-git-deploy-key']) {
              sh """
                # Git tag first
                git tag build-${BUILD_NUMBER}
                # Push tag back to GitLab
                SSH_AUTH_SOCK=${SSH_AUTH_SOCK} \
                  GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" \
                  git push --tags
              """
            }
          }
        }
      }
    }
  }
}
```

## Deploy to production Job

* Click on `New Item` on the left side and make sure you are in your `ratings` folder
* Enter an item name `deploy-prd` and choose `Pipeline` type
* Configure job
  * Advanced Project Options > Advanced...
    * Display Name: `Deploy to Production Environment`
  * Pipeline
    * Definition: `Pipeline script`
    * Script: `Pipeline script from SCM`
      * SCM: `Git`
        * Repository URL: `git@git.demo.opsta.co.th:dfd-[Y]/training[X]/ratings.git`
        * Credentials: `training[X]-bookinfo-git-deploy-key`
      * Script Path: `Jenkinsfile.prd`
* Create new file `Jenkinsfile.prd` to support deploy to production

```groovy
def scmVars

pipeline {
  agent {
    kubernetes {
      yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: helm
    image: lachlanevenson/k8s-helm:v3.0.2
    command:
    - cat
    tty: true
"""
    }
  }

  // Git Parameter Choices
  parameters {
    gitParameter name: 'TAG',
                 selectedValue: 'TOP',
                 sortMode: 'DESCENDING_SMART',
                 tagFilter: 'build-*',
                 type: 'PT_TAG'
  }

  stages {

    // ***** Stage Clone *****
    stage('Clone ratings source code') {
      steps {
        script {
          scmVars = checkout([
            $class: 'GitSCM',
            branches: [[name: "refs/tags/${params.TAG}"]],
            userRemoteConfigs: [[
              credentialsId: 'training[X]-bookinfo-git-deploy-key',
              url: 'git@git.demo.opsta.co.th:dfd-[Y]/training[X]/ratings.git'
            ]]
          ])
        }
      }
    }

    // ***** Stage Deploy *****
    stage('Deploy ratings with Helm Chart') {
      steps {
        container('helm') {
          script {
            withKubeConfig([credentialsId: 'gce-k8s-kubeconfig']) {
              // Helm upgrade with set production tag
              sh "helm upgrade -f k8s/helm-values/values-bookinfo-prd-ratings.yaml --wait \
                --set extraEnv.COMMIT_ID=${scmVars.GIT_COMMIT} \
                --set ratings.tag=${params.TAG} \
                --namespace training[X]-bookinfo-prd bookinfo-prd-ratings k8s/helm"
            }
          }
        }
      }
    }
  }
}
```

## Exercise: Details Service

* Do DevOps Flow from scratch with Details Service
