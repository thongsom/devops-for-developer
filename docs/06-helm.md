# Helm Workshop

## Add Helm stable Charts

```bash
# Add stable charts repository
helm repo add stable https://kubernetes-charts.storage.googleapis.com
# List repo
helm repo list
# Update repo
helm repo update
```

## Deploy basic MongoDB with Helm Charts from Kubeapps Hub

* Go to <https://hub.kubeapps.com/charts/stable/mongodb> to see MongoDB Helm Charts description
* Deploy basic MongoDB

```bash
# Check helm release
helm list
# Deploy MongoDB Helm Charts
helm install mongodb stable/mongodb
# Check helm release again
helm list
```

* Check if MongoDB is working

```bash
# Check resources
kubectl get pod
kubectl get deployment
kubectl get service
```

* Connect to MongoDB

```bash
# Check secret
kubectl get secret
kubectl describe secret mongodb

# Get MongoDB root password
export MONGODB_ROOT_PASSWORD=$(kubectl get secret mongodb \
  -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)
# Create another pod to connect to MongoDB via mongo cli command
kubectl run mongodb-client --rm --tty -i --restart='Never' --image bitnami/mongodb \
  --command -- mongo admin --host mongodb --authenticationDatabase admin -u root -p $MONGODB_ROOT_PASSWORD
show dbs
exit
```

## Deploy custom configuration MongoDB with Helm Value

### Prepare Helm Value file

* Check `values.yaml` on `https://github.com/helm/charts/tree/master/stable/mongodb` to see all configuration
* `mkdir ~/ratings/k8s/helm-values/` and create `values-bookinfo-dev-ratings-mongodb.yaml` file inside `helm-values` directory and put below content

```yaml
image:
  tag: 4.2.2-debian-9-r30
usePassword: false
persistence:
  enabled: false
initConfigMap:
  name: "bookinfo-dev-ratings-mongodb-initdb"
```

### Create initial script with Config Map

```bash
kubectl create configmap bookinfo-dev-ratings-mongodb-initdb \
  --from-file=databases/ratings_data.json \
  --from-file=databases/script.sh

# Check config map
kubectl get configmap
kubectl describe configmap bookinfo-dev-ratings-mongodb-initdb
```

### Deploy MongoDB with Helm Value file

```bash
# Delete first mongodb release
helm list
helm delete mongodb

# Deploy new MongoDB with Helm Value
helm install -f k8s/helm-values/values-bookinfo-dev-ratings-mongodb.yaml \
  bookinfo-dev-ratings-mongodb stable/mongodb
```

* Check if MongoDB is working properly with imported data

## Exercise: Update Ratings Service Manifest File

* Change Ratings Service Manifest File to read data from MongoDB

## Create Helm Chart for Ratings Service

* Delete current Ratings Service first with command `kubectl delete -f k8s/`
* `mkdir ~/ratings/k8s/helm` to create directory for Ratings Helm Charts
* Create `Chart.yaml` file inside `helm` directory and put below content

```yaml
apiVersion: v1
description: Bookinfo Ratings Service Helm Chart
name: bookinfo-ratings
version: 1.0.0
appVersion: 1.0.0
home: https://bookinfo.demo.opsta.co.th/training[X]/ratings
maintainers:
  - name: training[X]
    email: training[X]@opsta.in.th
sources:
  - https://git.demo.opsta.co.th/dfd-[Y]/training[X]/ratings.git
```

* `mkdir ~/ratings/k8s/helm/templates` to create directory for Helm Templates
* Move our ratings manifest file to template directory with command `mv k8s/ratings-*.yaml k8s/helm/templates/`
* Let's try deploy Ratings Service

```bash
# Deploy Ratings Helm Chart
helm install bookinfo-dev-ratings k8s/helm

# Get Status
kubectl get deployment
kubectl get pod
kubectl get service
kubectl get ingress
```

* Try to access <https://bookinfo.dev.opsta.co.th/training[X]/ratings/health> and <https://bookinfo.dev.opsta.co.th/training[X]/ratings/ratings/1> to check the deployment

## Create Helm Value file for Ratings Service

* Create `values-bookinfo-dev-ratings.yaml` file inside `k8s/helm-values` directory and put below content

```yaml
ratings:
  image: registry.demo.opsta.co.th/training[X]/bookinfo/ratings
  tag: dev
  replicas: 1
  imagePullSecrets: registry-bookinfo
  port: 8080
  healthCheckPath: "/health"
ingress:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /$2
  host: bookinfo.dev.opsta.co.th
  path: "/training[X]/ratings(/|$)(.*)"
  serviceType: ClusterIP
extraEnv:
  SERVICE_VERSION: v2
  MONGO_DB_URL: mongodb://bookinfo-dev-ratings-mongodb:27017/ratings
```

* Let's replace variable one-by-one with these object
  * {{ .Release.Name }}
  * {{ .Values.ratings.* }}
* This is sample syntax to have default value

```yaml
{{ .Values.ingress.path | default "/" }}
```

* This is a sample of using if and range syntax

```yaml
        {{- if .Values.extraEnv }}
        env:
        {{- range $key, $value := .Values.extraEnv }}
        - name: {{ $key }}
          value: {{ $value | quote }}
        {{- end }}
        {{- end }}
```

* This is sample syntax to loop annotation

```bash
  annotations:
  {{- range $key, $value := .Values.ingress.annotations }}
    {{ $key }}: {{ $value | quote }}
  {{- end }}
```

* After replace, you can upgrade release with below command

```bash
helm upgrade -f k8s/helm-values/values-bookinfo-dev-ratings.yaml \
  bookinfo-dev-ratings k8s/helm
```

## Exercise: Deploy on UAT and Production Environment

* Create Helm value and deploy for UAT and Production environment

Next: [Jenkins Workshop](07-jenkins.md)
