# Prerequisites

## Login to Cloud Shell

* Go to <https://console.cloud.google.com> and login with your credential

> Don't put any of recovery method. Just `Confirm`

* Make sure project name on the top left is `Opsta DevOps for Developer`

> If your language is not English. Go to <https://console.cloud.google.com/user-preferences/languages> and change language to `English (United States)`

* Click on `Activate Cloud Shell` icon on the top right
* Click on `Open in new window` icon on the top right of Cloud Shell to open Cloud Shell in new tab

## Reset Cloud Shell

* Put this command to remove all the files in your $HOME directory

```bash
sudo rm -rf $HOME
```

* Click on `vertical three dot` icon on the top right for more menu and choose `Restart`

## Prepare SSH key

* Put this command to generate your SSH key

```bash
ssh-keygen -f ~/.ssh/id_rsa -N ""
```

> You will have your SSH private and public key in `~/.ssh/` directory

## Install Docker Compose

```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.25.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose version
```

## Install Helm

```bash
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
helm version
```

Next: [Git and GitLab Workshop](02-git.md)
