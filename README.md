# Opsta DevOps Bootcamp

This tutorial walks you through setting up DevOps Flow.

## Copyright

Opsta (Thailand) Co.,Ltd.

## DevOps Tools Details

| Tools           | Description                                                                                                                                                                                              |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GitLab          | <https://git.demo.opsta.co.th><br/>user: training1, training2, … , training20<br/>pass: OpstaThailand                                                                                                                    |
| Jenkins         | <https://jenkins.demo.opsta.co.th><br/>user: bookinfo<br/>pass: OpstaThailand                                                                                                                                             |
| Nexus           | <https://nexus.demo.opsta.co.th> for Artifacts Server<br/><https://registry.demo.opsta.co.th> for Docker Private Registry<br/>user: bookinfo<br/>Pass: OpstaThailand                                |
| Cloud Shell     | <https://console.cloud.google.com><br/>user: training[X]@opsta.in.th<br/>pass: OpstaThailand                                                                                                                    |
| Bookinfo Domain | <http://bookinfo.opsta.co.th/training[X]/productpage><br/><http://bookinfo.opsta.co.th/training[X]/reviews><br/><http://bookinfo.opsta.co.th/training[X]/details><br/><http://bookinfo.opsta.co.th/training[X]/ratings> |

## Workshop

This tutorial assumes you have access to the [Google Cloud Platform](https://cloud.google.com), have all the DevOps Tools installed and running (GitLab, Jenkins, Nexus, Kubernetes Cluster) and know about how Bookinfo Application working.

* [Prerequisites](docs/01-prerequisites.md)
* [Git and GitLab Workshop](docs/02-git.md)
* [Docker Workshop](docs/03-docker.md)
* [Kubernetes Commands Workshop](docs/04-kubernetes-commands.md)
* [Kubernetes Manifest Files Workshop](docs/05-kubernetes-manifest.md)
* [Helm Workshop](docs/06-helm.md)
* [Jenkins Workshop](docs/07-jenkins.md)
